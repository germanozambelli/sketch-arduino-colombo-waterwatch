/*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ______      __                __             _       __      __               _       __      __       __
  / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
/ /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
\____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/

 Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli

                    __      __         __
                   /  \    /  \_____ _/  |_  ___________
                   \   \/\/   /\__  \\   __\/ __ \_  __ \
                    \        /  / __ \|  | \  ___/|  | \/
                     \__/\  /  (____  /__|  \___  >__|
                          \/        \/          \/
             __      __         __         .__
            /  \    /  \_____ _/  |_  ____ |  |__   ___________
            \   \/\/   /\__  \\   __\/ ___\|  |  \_/ __ \_  __ \
             \        /  / __ \|  | \  \___|   Y  \  ___/|  | \/
              \__/\  /  (____  /__|  \___  >___|  /\___  >__|
                   \/        \/          \/     \/     \/
                      _______________  ____ .________
                      \_____  \   _  \/_   ||   ____/
                       /  ____/  /_\  \|   ||____  \
                      /       \  \_/   \   |/       \
                      \_______ \_____  /___/______  /
                              \/     \/           \/

  project: WaterWatcher
  author: Andrea Cazzadori (mrwolf.fablabimperia.org)
  license: WTFPL
  file: station/log.cpp
  modified: 2015/06/02 - 15:25:45
  header created: 2015/06/02 - 16:17:07
_____________________________________________________________________________________________________________________________________________

              DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
______________________________________________________________________________________________________________________________________________

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; BUSINESS
INTERRUPTION; OR INJURIES, DEATHS OR DAMAGE TO PROPERTIES) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

#include <string.h>
#include "clock.h"
#include "dbg.h"
#include "device.h"
#include "cfg.h"
//nuove librerie sim900

#include "inetGSM.h"
InetGSM inet;

char msg[50];
int numdata;
char inSerial[50];
int i=0;
boolean started=false;
String stringOne, stringTwo;
String url;
char charBuf;

static Clock _ck;

static uint32 _failed_logs = 0;

/*
========================================
logData
send values to the server via GPRS
========================================
*/

bool logData( )
{
    bool res = false;

    
    if( _ck.seconds( ) < CFG_LOG_TIME_SECONDS )
    {
        return true;
    }
    _ck.reset( );

    DBGLN( "log data" );

              //TCP Client GET, send a GET request to the server and
          //save the reply.
        

  

    // save value, restore if log fails
#if CFG_SENSOR_RAIN_GAUGE_ENABLED == true
    float rain = SensorRainGauge::readReset( );
#endif

                  //TCP Client GET, send a GET request to the server and
          //save the reply.
         #define QS_MAX_LENS 128
    char qss[ QS_MAX_LENS ];
    sizet ns = 0;

    ns += sprintf( qss + ns, "%s", "/api/");
    ns += sprintf( qss + ns, "%s", CFG_SERVER_API_KEY );
    ns += sprintf( qss + ns, "%s", "/measurement/add/");
    ns += sprintf( qss + ns, "%s", "{\"temp\":");
    ns += sprintf( qss + ns, "%d", int32(100 * SensorTemperature::tAvg( ) ));
    ns += sprintf( qss + ns, "%s", ",\"hydrometric\":");
    ns += sprintf( qss + ns, "%d", int32(100 * SensorSonar::levelAvg( ) ));
    ns += sprintf( qss + ns, "%s", ",\"rain\":");
    ns += sprintf( qss + ns, "%d", int(100 * rain ));
    ns += sprintf( qss + ns, "%s", ",\"battery\":");
    ns += sprintf( qss + ns, "%d", int32(100 * SensorBatteryVoltage::chargeLeft() ));
   

    ns += sprintf( qss + ns, "%s", "%7D");
    Serial.println(qss);

  
//Serial connection.
  
     Serial.println("GSM Shield testing.");
     //Start configuration of shield with baudrate.
     //For http uses is raccomanded to use 4800 or slower.
     if (gsm.begin(2400)) {
          Serial.println("\nstatus=READY");
          started=true;
     } else Serial.println("\nstatus=IDLE");

     if(started) {
          //GPRS attach, put in order APN, username and password.
          //If no needed auth let them blank.
          if (inet.attachGPRS(CFG_INTERNET_APN, "", ""))
               Serial.println("status=ATTACHED");
          else Serial.println("status=ERROR");
          delay(1000);

          //Read IP address.
          gsm.SimpleWriteln("AT+CIFSR");
          delay(5000);
          //Read until serial buffer is empty.
          gsm.WhileSimpleRead();

          
          numdata=inet.httpGET(CFG_SERVER_NAME, 80, qss, msg, 50);
       
          //Print the results.
          Serial.println("\nNumber of data received:");
          Serial.println(numdata);
          Serial.println("\nData received:");
          Serial.println(msg);
     }


    
    if( ! res )
    {
#if CFG_SENSOR_RAIN_GAUGE_ENABLED == true
        SensorRainGauge::restore( rain );
        ++ _failed_logs;
#endif
    }
    else
    {

#if CFG_SENSOR_SONAR_ENABLED == true
        SensorSonar::reset( );
#endif
#if CFG_SENSOR_TEMPERATURE_ENABLED == true
        SensorTemperature::reset( );
#endif
#if CFG_SENSOR_BATTERY_VOLTAGE_ENABLED == true
        SensorBatteryVoltage::reset( );
#endif
        _failed_logs = 0;
    }

    return res;
}
